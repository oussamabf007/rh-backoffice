import { useState } from "react";
import Loading from "../../../components/Loading";
import API from "../../../utils/API";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import { useHistory } from "react-router-dom";
import {
  Button,
  Grid,
  Paper,
  TextField,
  InputLabel,
  Select,
  MenuItem,
} from "@material-ui/core";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";

import FormLabel from "@mui/material/FormLabel";

const AddDeveloper = () => {
  const [LoadingOpen, setLoadingOpen] = useState(false);

  const history = useHistory();

  const [selectedDate, setSelectedDate] = useState(new Date());

  const handleSelectedDateChange = (date) => {
    setSelectedDate(date);
  };

  const datetosave = selectedDate;

  const onSubmitHandler = async (e) => {
    e.preventDefault();
    const fname = e.target.fname.value;
    const lname = e.target.lname.value;
    const email = e.target.email.value;
    const gender = e.target.gender.value;
    const adresse = e.target.adresse.value;
    const poste = e.target.poste.value;
    const cin = e.target.cin.value;
    const rib = e.target.rib.value;
    const salaire = e.target.salaire.value;
    const contrat = e.target.contrat.value;

    if (!fname && !lname && !email && !gender && !adresse) {
      alert("il faut remplir le champ");
      return false;
    }

    try {
      setLoadingOpen(true);
      const res = await API(
        "POST",
        `${process.env.REACT_APP_API_BASE_URL}/developers`,
        {
          fname,
          lname,
          email,
          gender,
          adresse,
          contrat,
          poste,
          cin,
          rib,
          salaire,
          birthday: datetosave,
        }
      );
      if (res.code === 200) {
        alert("employé ajouté avec succés");
        history.push("/developers");
      } else {
        alert(
          "Une erreur est survenue, Veuillez réessayer, si l'erreur persiste contacter l'administrateur du site."
        );
      }
    } catch (error) {
      setLoadingOpen(false);
      console.log(error);
    }
  };
  return (
    <>
      <Loading open={LoadingOpen} />
      <Grid justify="center" alignItems="center" container spacing={3}>
        <Grid item xs={6}>
          <h2>Ajouter un developpeur </h2>
          <Paper
            style={{
              padding: "20px",
            }}
          >
            <form onSubmit={onSubmitHandler}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Prénom"
                    name="fname"
                    fullWidth
                  />
                </div>

                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Nom"
                    name="lname"
                    fullWidth
                  />
                </div>

                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Email"
                    name="email"
                    fullWidth
                  />
                </div>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Adresse"
                    name="adresse"
                    fullWidth
                  />
                </div>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Poste"
                    name="poste"
                    fullWidth
                  />
                </div>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Cin"
                    name="cin"
                    fullWidth
                  />
                </div>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Rib"
                    name="rib"
                    fullWidth
                  />
                </div>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Salaire"
                    name="salaire"
                    fullWidth
                  />
                </div>
                <div
                  align="center"
                  style={{ marginBottom: "20px", minWidth: 300 }}
                >
                  <InputLabel id="demo-simple-select-label">Contrat</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    label="Contrat"
                    name="contrat"
                  >
                    <MenuItem value={10}>Sivip</MenuItem>
                    <MenuItem value={20}>Cdd</MenuItem>
                    <MenuItem value={30}>Cdi</MenuItem>
                  </Select>
                </div>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <FormLabel>Gender</FormLabel>
                  <RadioGroup
                    aria-label="gender"
                    defaultValue="female"
                    name="gender"
                  >
                    <FormControlLabel
                      value="female"
                      control={<Radio />}
                      label="Female"
                    />
                    <FormControlLabel
                      value="male"
                      control={<Radio />}
                      label="Male"
                    />
                  </RadioGroup>
                </div>

                <div style={{ marginBottom: "20px" }}>
                  <KeyboardDatePicker
                    margin="normal"
                    id="date-picker-dialog"
                    label="Date"
                    fullWidth
                    format="dd/MM/yyyy"
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    value={selectedDate}
                    onChange={handleSelectedDateChange}
                  />
                </div>

                <div align="center">
                  <Button variant="contained" color="primary" type="submit">
                    Ajouter
                  </Button>
                </div>
              </MuiPickersUtilsProvider>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};
export default AddDeveloper;
