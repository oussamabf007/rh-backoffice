import { useState, useEffect } from "react";
import Loading from "../../../components/Loading";
import API from "../../../utils/API";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import { useHistory, useParams } from "react-router-dom";
import { Button, Grid, Paper, TextField } from "@material-ui/core";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";

import FormLabel from "@mui/material/FormLabel";

const EditDeveloper = () => {
  const { id } = useParams();
  const [LoadingOpen, setLoadingOpen] = useState(false);

  const history = useHistory();

  const [selectedDate, setSelectedDate] = useState(new Date());

  const [data, setData] = useState({
    fname: "",
    lname: "",
    email: "",
    gender: "",
    adresse: "",
  });

  const handleSelectedDateChange = (date) => {
    setSelectedDate(date);
  };

  const datetosave = selectedDate;

  useEffect(() => {
    const fecthData = async (e) => {
      const res = await API(
        "GET",
        `${process.env.REACT_APP_API_BASE_URL}/developers/${id}`
      );
      console.log("res.data", res.data);
      if (res.code === 200) {
        setData({
          fname: res.data.fname,
          lname: res.data.lname,
          email: res.data.email,
          gender: res.data.gender,
          adresse: res.data.adresse,
        });
      }
    };
    fecthData();
    return () => {
      setData([]);
    };
  }, [id]);

  const handleFormChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  const onSubmitHandler = async (e) => {
    e.preventDefault();
    const fname = e.target.fname.value;
    const lname = e.target.lname.value;
    const email = e.target.email.value;
    const gender = e.target.gender.value;
    const adresse = e.target.adresse.value;

    if (!fname && !lname && !email && !gender && !adresse) {
      alert("il faut remplir le champ");
      return false;
    }

    try {
      setLoadingOpen(true);
      const res = await API(
        "PUT",
        `${process.env.REACT_APP_API_BASE_URL}/developers/${id}`,
        { fname, lname, email, gender, adresse, birthday: datetosave }
      );
      if (res.code === 200) {
        alert("employé modifié avec succés");
        history.push("/developers");
      } else {
        alert(
          "Une erreur est survenue, Veuillez réessayer, si l'erreur persiste contacter l'administrateur du site."
        );
      }
    } catch (error) {
      setLoadingOpen(false);
      console.log(error);
    }
  };

  return (
    <>
      <Loading open={LoadingOpen} />
      <Grid justify="center" alignItems="center" container spacing={3}>
        <Grid item xs={6}>
          <h2>Ajouter un developpeur </h2>
          <Paper
            style={{
              padding: "20px",
            }}
          >
            <form onSubmit={onSubmitHandler}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Prénom"
                    name="fname"
                    value={data.fname ?? ""}
                    onChange={handleFormChange}
                    fullWidth
                  />
                </div>

                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Nom"
                    name="lname"
                    value={data.lname ?? ""}
                    onChange={handleFormChange}
                    fullWidth
                  />
                </div>

                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Email"
                    name="email"
                    value={data.email ?? ""}
                    onChange={handleFormChange}
                    fullWidth
                  />
                </div>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Adresse"
                    name="adresse"
                    value={data.adresse ?? ""}
                    onChange={handleFormChange}
                    fullWidth
                  />
                </div>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <FormLabel>Gender</FormLabel>
                  <RadioGroup
                    aria-label="gender"
                    defaultValue="female"
                    name="gender"
                    value={data.gender ?? ""}
                    onChange={handleFormChange}
                  >
                    <FormControlLabel
                      value="female"
                      control={<Radio />}
                      label="Female"
                    />
                    <FormControlLabel
                      value="male"
                      control={<Radio />}
                      label="Male"
                    />
                    <FormControlLabel
                      value="other"
                      control={<Radio />}
                      label="Other"
                    />
                  </RadioGroup>
                </div>

                <div style={{ marginBottom: "20px" }}>
                  <KeyboardDatePicker
                    margin="normal"
                    id="date-picker-dialog"
                    label="Date"
                    fullWidth
                    format="dd/MM/yyyy"
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    value={selectedDate}
                    onChange={handleSelectedDateChange}
                  />
                </div>

                <div align="center">
                  <Button variant="contained" color="primary" type="submit">
                    Ajouter
                  </Button>
                </div>
              </MuiPickersUtilsProvider>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};
export default EditDeveloper;
