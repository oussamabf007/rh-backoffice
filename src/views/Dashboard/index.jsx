import React, { useEffect, useState } from "react";
import API from "../../utils/API";
import TableTask from "../../components/TableTask";

const Dashbord = () => {
  const [data, setData] = useState([]);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await API(
          "GET",
          `${process.env.REACT_APP_API_BASE_URL}/admin/conges`
        );
        setData(res.data);
        // console.log(res);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, [refresh]);

  const handleRefreshCallback = () => {
    setRefresh(!refresh);
  };

  return (
    <>
      <div style={{ marginTop: "50px" }}>
        <TableTask
          data={data}
          title="Conges demandé"
          redirectUri="/"
          apiUrl="/admin/conges/"
          refreshCallback={handleRefreshCallback}
        />
      </div>
    </>
  );
  /* <>
      <div>
        {data &&
          Object.keys(data).map((developer, i) => (
            <div>
              <h3 style={{ color: "red" }}>{developer}</h3>
              <table>
                <thead style={{ margin: "20px" }}>
                  <tr>
                    <th>date</th>

                    <th>nbjour</th>
                    <th>accepted</th>
                  </tr>
                </thead>
                <tbody>
                  {data[developer].map((conges, i) => (
                    <tr>
                      <td>
                        <FormatDate date={conges.date.date} />
                      </td>
                      <td>{conges.nbjour}</td>
                      <td>{`${conges.accepted}`}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ))}
      </div>
    </> */
};

export default Dashbord;
