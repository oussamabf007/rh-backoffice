import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import API from "../../utils/API";
import { Button } from "@material-ui/core";
import TableTask from "../../components/TableTask";
const Demande = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await API(
          "GET",
          `${process.env.REACT_APP_API_BASE_URL}/user/conges`
        );
        setData(res.data);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);

  return (
    <>
      <Link to="/demande/new">
        <Button
          color="primary"
          style={{
            border: "1px solid",
            width: "200px",
            position: "absolute",
            right: "10px",
          }}
        >
          Faire une demande
        </Button>
      </Link>
      <div style={{ marginTop: "50px" }}>
        <TableTask
          data={data}
          apiUrl={`${process.env.REACT_APP_API_BASE_URL}/user/conges`}
          title="Mes congés"
          redirectUri="/demande/"
        />
      </div>
    </>
  );
};

export default Demande;
