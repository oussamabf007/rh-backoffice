import { useState } from "react";
import Loading from "../../../components/Loading";
import API from "../../../utils/API";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import { useHistory } from "react-router-dom";
import { Button, Grid, Paper, TextField } from "@material-ui/core";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";

import FormLabel from "@mui/material/FormLabel";

const AddDemande = () => {
  const [LoadingOpen, setLoadingOpen] = useState(false);

  const history = useHistory();

  const [selectedDate, setSelectedDate] = useState(new Date());

  const handleSelectedDateChange = (date) => {
    setSelectedDate(date);
  };

  const datetosave = selectedDate;

  const onSubmitHandler = async (e) => {
    e.preventDefault();
    const nbjour = e.target.nbjour.value;
    const nbheure = e.target.nbheure.value;

    try {
      setLoadingOpen(true);
      const res = await API(
        "POST",
        `${process.env.REACT_APP_API_BASE_URL}/user/conges`,
        {
          nbjour,
          nbheure,
          date: datetosave,
        }
      );
      if (res.code === 200) {
        alert("demande envoyé avec succéé");
        history.push("/demandes");
      } else {
        alert(
          "Une erreur est survenue, Veuillez réessayer, si l'erreur persiste contacter l'administrateur du site."
        );
      }
    } catch (error) {
      setLoadingOpen(false);
      console.log(error);
    }
  };

  return (
    <>
      <Loading open={LoadingOpen} />
      <Grid justify="center" alignItems="center" container spacing={3}>
        <Grid item xs={6}>
          <h2>Faire une demande </h2>
          <Paper
            style={{
              padding: "20px",
            }}
          >
            <form onSubmit={onSubmitHandler}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Nombre de jours"
                    name="nbjour"
                    fullWidth
                  />
                </div>
                <div align="center" style={{ marginBottom: "20px" }}>
                  <TextField
                    id="standard-basic"
                    label="Nombre d'heures"
                    name="nbheure"
                    fullWidth
                  />
                </div>

                <div style={{ marginBottom: "20px" }}>
                  <KeyboardDatePicker
                    margin="normal"
                    id="date-picker-dialog"
                    label="Date"
                    fullWidth
                    format="dd/MM/yyyy"
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                    value={selectedDate}
                    onChange={handleSelectedDateChange}
                  />
                </div>

                <div align="center">
                  <Button variant="contained" color="primary" type="submit">
                    Ajouter
                  </Button>
                </div>
              </MuiPickersUtilsProvider>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default AddDemande;
