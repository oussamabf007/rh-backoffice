import React, { Component } from "react";
import axios from "axios";
class App extends Component {
  viewHandler = async () => {
    var authorization = "";
    const user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      authorization = user.token;
    }
    axios(`${process.env.REACT_APP_API_BASE_URL}/admin/contrat`, {
      method: "GET",
      headers: {
        Authorization: "bearer " + authorization,
        "Content-Type": "application/pdf",
      },
    })
      .then((res) => res.blob())
      .then((response) => {
        //Create a Blob from the PDF Stream
        console.log(response);
        const file = new Blob([response], {
          type: "application/pdf",
        });
        //Build a URL from the file
        const fileURL = URL.createObjectURL(file);
        //Open the URL on new Window
        window.open(fileURL);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    return (
      <div>
        <button onClick={this.viewHandler}> View Pdf </button>{" "}
      </div>
    );
  }
}
export default App;
