import { useEffect, useState } from "react";
import Loading from "../../../components/Loading";
import API from "../../../utils/API";
import { useHistory } from "react-router-dom";
import {
  Button,
  Grid,
  MenuItem,
  Paper,
  Select,
  TextField,
  InputLabel,
  FormControl,
} from "@material-ui/core";

const AddTask = () => {
  const [LoadingOpen, setLoadingOpen] = useState(false);
  const [type, setType] = useState([]);

  const history = useHistory();
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await API(
          "GET",
          `${process.env.REACT_APP_API_BASE_URL}/types`
        );
        setType(res.data);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);

  const onSubmitHandler = async (e) => {
    e.preventDefault();
    const name = e.target.name.value;
    const description = e.target.description.value;
    const type = e.target.type.value;

    if (!name && !description) {
      alert("il faut remplir le champ");
      return false;
    }
    try {
      setLoadingOpen(true);
      const res = await API(
        "POST",
        `${process.env.REACT_APP_API_BASE_URL}/tasks`,
        { name, description, type }
      );
      if (res.code === 200) {
        alert("Tache ajouté avec succés");
        history.push("/tasks");
      } else {
        alert(
          "Une erreur est survenue, Veuillez réessayer, si l'erreur persiste contacter l'administrateur du site."
        );
      }
    } catch (error) {
      setLoadingOpen(false);
      console.log(error);
    }
  };

  return (
    <>
      <Loading open={LoadingOpen} />
      <Grid justify="center" alignItems="center" container spacing={3}>
        <Grid item xs={6}>
          <h2>Ajouter Tache </h2>
          <Paper
            style={{
              padding: "20px",
            }}
          >
            <form onSubmit={onSubmitHandler}>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Nom"
                  name="name"
                  fullWidth
                />
              </div>
              <div align="center" style={{ marginBottom: "20px" }}>
                <TextField
                  id="standard-basic"
                  label="Déscription"
                  name="description"
                  fullWidth
                />
              </div>
              <FormControl fullWidth style={{ marginBottom: "20px" }}>
                <InputLabel id="location_label">Type</InputLabel>
                <Select labelId="type_label" id="typeId" name="type" fullWidth>
                  {type.map((item) => (
                    <MenuItem value={item.id}>{item.name}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <div align="center">
                <Button variant="contained" color="primary" type="submit">
                  Ajouter
                </Button>
              </div>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default AddTask;
