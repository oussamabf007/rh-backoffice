import { Switch, Redirect } from "react-router-dom";

import DefaultLayout from "../layouts/DefaultLayout";
import NotFound from "../layouts/404";

import { Route } from "react-router-dom";

import Dashboard from "../views/Dashboard";
import Developer from "../views/Developer";
import AddDeveloper from "../views/Developer/AddDeveloper";
import EditDeveloper from "../views/Developer/EditDeveloper";
import Login from "../views/Login";

import Settings from "../views/Settings";
import RouteWithLayout from "./RouteWithLayout";
import Demande from "../views/Demande";
import AddDemande from "../views/Demande/AddDemande";
import Contrat from "../views/Contrat";

const Routes = () => {
  return (
    <Switch>
      <RouteWithLayout
        component={Dashboard}
        exact
        layout={DefaultLayout}
        path="/"
      />
      <RouteWithLayout
        component={Developer}
        exact
        layout={DefaultLayout}
        path="/developers"
      />
      <RouteWithLayout
        component={AddDeveloper}
        exact
        layout={DefaultLayout}
        path="/developers/new"
      />
      <RouteWithLayout
        component={EditDeveloper}
        exact
        layout={DefaultLayout}
        path="/developers/:id/update"
      />

      <RouteWithLayout
        component={Settings}
        layout={DefaultLayout}
        exact
        path="/settings"
      />

      <RouteWithLayout
        component={Demande}
        layout={DefaultLayout}
        exact
        path="/demandes"
      />

      <RouteWithLayout
        component={AddDemande}
        layout={DefaultLayout}
        exact
        path="/demande/new"
      />

      <RouteWithLayout
        component={Contrat}
        layout={DefaultLayout}
        exact
        path="/contrat"
      />
      <Route component={NotFound} exact path="/not-found" />
      <Route component={Login} exact path="/login" />

      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
